package com.dkit.gd2.vilandasmorrissey.core;

/**
 *
 * @author Vilandas Morrissey D00218436
 */
public class ServiceDetails
{
    public final static int LISTENING_PORT = 50000;
    private final int SYSTEM_COMMAND_LENGTH = 2;

    public enum Menu
    {
        SAVE_LOCAL_TOLLS_AND_QUIT,
        UPDATED_VALID_VEHICLES_LIST,
        HEARTBEAT,
        REGISTER_TOLL_EVENT,
        PROCESS_TOLL_EVENT_BILLS;

        public String getName()
        {
            String name = name();
            name = name.replaceAll("_", " ");
            return name.charAt(0) + name.substring(1).toLowerCase();
        }
    }

    //Enums: Command("Response")
    public enum ECommands
    {
        CLOSE(""),
        GET_REGISTERED_VEHICLES("ReturnRegisteredVehicles"),
        HEARTBEAT("Heartbeat response"),
        REGISTER_VALID_TOLL_EVENT("RegisteredValidTollEvent"),
        REGISTER_INVALID_TOLL_EVENT("RegisteredInvalidTollEvent"),
        UNRECOGNISED("Error: Unrecognised Command"),
        SERVER_ERROR("Server could not connect to Database");

        private String response;

        ECommands(String response)
        {
            this.response = response;
        }

        public String getName()
        {
            String name = name();
            name = name.replaceAll("_", " ");
            return name.charAt(0) + name.substring(1).toLowerCase();
        }

        public String getResponse()
        {
            return response;
        }
    }
}
