package com.dkit.gd2.vilandasmorrissey.DTOs;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Vilandas Morrissey D00218436
 */
public class CustomerTest
{
    Customer customer;

    public CustomerTest()
    {
    }
    

    @BeforeClass
    public static void setUpClass()
    {
    }
    

    @AfterClass
    public static void tearDownClass()
    {
    }
    

    @Before
    public void setUp()
    {
        customer = new Customer("John Wick", "123 Home Place");
    }
    

    @After
    public void tearDown()
    {
    }


    @Test
    public void testBillingCar()
    {
        System.out.println("\nTesting billing car:");
        double expected = 1.90;
        double cost = 1.90;
        customer.addToll("M50", "Car", "XXXX", cost);

        assertEquals(cost, customer.getTotalCostForTesting(), 0.0001);
    }

    @Test
    public void testBilling2Cars()
    {
        System.out.println("\nTesting billing 2 cars:");
        double expected = 3.80;
        double cost = 1.90;
        customer.addToll("M50", "Car", "XXXX", cost);
        customer.addToll("M50", "Car", "XXXX", cost);

        assertEquals(expected, customer.getTotalCostForTesting(), 0.0001);
    }

    @Test
    public void testBilling2Vehicles()
    {
        System.out.println("\nTesting billing 2 vehicles:");
        double expected = 5.30;
        double costCar = 1.90;
        double costTruck = 3.40;
        customer.addToll("M50", "Car", "XXXX", costCar);
        customer.addToll("M50", "Car", "YYYY", costTruck);

        assertEquals(expected, customer.getTotalCostForTesting(), 0.0001);
    }

    @Test
    public void testBilling5Vehicles()
    {
        System.out.println("\nTesting billing 5 vehicles:");

        double expected = 15.40;
        double costCar = 1.90;
        double costTruck = 3.40;
        double costBus = 4.80;

        customer.addToll("M50", "Car", "XXXX", costCar);
        customer.addToll("M50", "Truck", "YYYY", costTruck);
        customer.addToll("M50", "Bus", "ZZZZ", costBus);
        customer.addToll("M50", "Truck", "YXYY", costTruck);
        customer.addToll("M50", "Car", "XYYX", costCar);

        assertEquals(expected, customer.getTotalCostForTesting(), 0.0001);
    }
    

}
