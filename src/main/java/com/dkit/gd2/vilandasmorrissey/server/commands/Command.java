package com.dkit.gd2.vilandasmorrissey.server.commands;

import com.dkit.gd2.vilandasmorrissey.core.ServiceDetails.ECommands;

/**
 *
 * @author Vilandas Morrissey D00218436
 */
public interface Command
{
    public String createResponse(ECommands command, Object param);
}
