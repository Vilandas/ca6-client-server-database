package com.dkit.gd2.vilandasmorrissey.Daos;

import com.dkit.gd2.vilandasmorrissey.DTOs.Customer;
import com.dkit.gd2.vilandasmorrissey.DTOs.TollEvent;
import com.dkit.gd2.vilandasmorrissey.DTOs.Vehicle;
import com.dkit.gd2.vilandasmorrissey.Exceptions.DaoException;

import java.util.HashMap;
import java.util.List;

/**
 * Declares the methods that all tollsDAO types must implement
 * @author Vilandas Morrissey D00218436
 */
public interface ITollsDaoInterface
{
    List<Vehicle> getRegisteredVehicles() throws DaoException;
    HashMap<Integer, Customer> getUnprocessedTolls() throws DaoException;
    void markTollsAsProcessed() throws DaoException;
    void registerTollEvent(TollEvent event) throws DaoException;
    void registerVehicleRegistration(String registration, boolean valid) throws DaoException;



}
