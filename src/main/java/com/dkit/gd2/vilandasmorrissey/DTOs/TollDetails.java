package com.dkit.gd2.vilandasmorrissey.DTOs;

/**
 *
 * @author Vilandas Morrissey D00218436
 */
public class TollDetails
{
    private String boothID;
    private String vehicleType;
    private String registration;
    private double cost;

    public TollDetails(String boothID, String vehicleType, String registration, double cost)
    {
        this.boothID = boothID;
        this.vehicleType = vehicleType;
        this.registration = registration;
        this.cost = cost;
    }

    public double getCost()
    {
        return cost;
    }

    @Override
    public String toString()
    {
        return String.format("%-12s %-15s %-20s €%-6.2f", boothID, vehicleType, registration, cost);
    }
}
