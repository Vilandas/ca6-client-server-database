package com.dkit.gd2.vilandasmorrissey.DTOs;

/**
 *
 * @author Vilandas Morrissey D00218436
 */
public class Vehicle
{
    private String registry;
    private boolean valid;

    public Vehicle(String registry, boolean valid)
    {
        this.registry = registry;
        this.valid = valid;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Vehicle vehicle = (Vehicle) o;

        return registry != null ? registry.equals(vehicle.registry) : vehicle.registry == null;
    }

    @Override
    public int hashCode()
    {
        return registry != null ? registry.hashCode() : 0;
    }

    public String getRegistry()
    {
        return this.registry;
    }

    public boolean isValid()
    {
        return this.valid;
    }
}
