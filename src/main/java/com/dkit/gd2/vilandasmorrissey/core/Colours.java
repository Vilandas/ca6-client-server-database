package com.dkit.gd2.vilandasmorrissey.core;

/**
 *
 * @author Vilandas Morrissey D00218436
 */
public class Colours
{
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_GREEN = "\u001B[32m";
}
