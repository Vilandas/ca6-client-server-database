package com.dkit.gd2.vilandasmorrissey.DTOs;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 *
 * @author Vilandas Morrissey D00218436
 */
public class TollEvent implements Serializable
{
    private long serialVersionUID = 1L;

    private String tollboothID;
    private String vehicleReg;
    private long imageID;
    private Timestamp time;
    private boolean valid;
    private int vehicleID;

    public TollEvent(String tollboothID, String vehicleReg, long imageID, Timestamp time)
    {
        this.tollboothID = tollboothID;
        this.vehicleReg = vehicleReg;
        this.imageID = imageID;
        this.time = time;
        this.valid = false;
    }

    public String getTollboothID()
    {
        return tollboothID;
    }

    public String getVehicleReg()
    {
        return vehicleReg;
    }

    public long getImageID()
    {
        return imageID;
    }

    public Timestamp getTime()
    {
        return time;
    }

    public boolean isValid()
    {
        return valid;
    }

    public int getVehicleID()
    {
        return this.vehicleID;
    }

    public void setVehicleID(int vehicleID)
    {
        this.vehicleID = vehicleID;
    }

    @Override
    public String toString()
    {
        return String.format("%-12s %-12s %-10s %-24s %s", tollboothID, vehicleReg, imageID, time);
    }
}
