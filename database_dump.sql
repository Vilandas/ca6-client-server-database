-- MySQL dump 10.16  Distrib 10.1.37-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: oopca6
-- ------------------------------------------------------
-- Server version	10.1.37-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(50) DEFAULT NULL,
  `customer_address` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` VALUES (1,'Lacresha Lunsford','452 Rose St. Buckeye, AZ 85326'),(2,'Todd Champion','34 Trusel Street Davenport, IA 52804'),(3,'Valrie Ennals','9720 New St. Cumberland, RI 02864'),(4,'Nakisha Gormley','69 South Harrison Dr. Asbury Park, NJ 07712'),(5,'Charline Ptacek','71 Chestnut Rd. Longview, TX 75604'),(6,'Desire Randazzo','8410 Grandrose Court Ankeny, IA 50023'),(7,'Sherley Sarinana','8303 Roosevelt St. Dorchester, MA 02125'),(8,'Adalberto Kleist','8871 North Berkshire Drive Park Forest, IL 60466'),(9,'Buck Cecil','289 East Oak Valley St. Southfield, MI 48076'),(10,'Reid Weatherford','77 Vernon Court Ooltewah, TN 37363'),(11,'Trish Rhyne','456 Henry Drive Maineville, OH 45039'),(12,'Junita Driscoll','9832 West Court Long Beach, NY 11561'),(13,'Andy Labelle','9191 8th St. Providence, RI 02904'),(14,'Svetlana Stuber','9397 Warren St. Wake Forest, NC 27587'),(15,'Drema Paro','85 S. Littleton Lane Glendale Heights, IL 60139'),(16,'Randolph Wymer','8225 South Helen Avenue Staunton, VA 24401'),(17,'Mauricio Sweatman','322 Beech Ave. Severna Park, MD 21146'),(18,'Sebastian Ungar','78 W. Constitution Rd. Traverse City, MI 49684'),(19,'Shala Delaune','9932 East Wild Horse Ave. Royal Oak, MI 48067'),(20,'Harvey Matsuura','42 Randall Mill Court Kenosha, WI 53140');
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_vehicle`
--

DROP TABLE IF EXISTS `customer_vehicle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_vehicle` (
  `customer_id` int(11) DEFAULT NULL,
  `vehicle_id` int(11) DEFAULT NULL,
  KEY `customer_id` (`customer_id`),
  KEY `vehicle_id` (`vehicle_id`),
  CONSTRAINT `customer_vehicle_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`customer_id`),
  CONSTRAINT `customer_vehicle_ibfk_2` FOREIGN KEY (`vehicle_id`) REFERENCES `vehicle` (`vehicle_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_vehicle`
--

LOCK TABLES `customer_vehicle` WRITE;
/*!40000 ALTER TABLE `customer_vehicle` DISABLE KEYS */;
INSERT INTO `customer_vehicle` VALUES (12,1),(8,2),(7,3),(7,4),(13,5),(18,6),(5,7),(13,8),(8,9),(3,10),(14,11),(4,12),(2,13),(15,14),(7,15),(10,16),(6,17),(17,18),(20,19),(10,20),(11,21),(1,22),(6,23),(19,24),(9,25),(17,26),(13,27),(1,28),(16,29),(18,30);
/*!40000 ALTER TABLE `customer_vehicle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `toll_event`
--

DROP TABLE IF EXISTS `toll_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `toll_event` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `vehicle_id` int(11) DEFAULT NULL,
  `tollbooth_id` varchar(12) DEFAULT NULL,
  `image_id` bigint(20) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `processed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`event_id`),
  KEY `vehicle_id` (`vehicle_id`),
  CONSTRAINT `toll_event_ibfk_1` FOREIGN KEY (`vehicle_id`) REFERENCES `vehicle` (`vehicle_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `toll_event`
--

LOCK TABLES `toll_event` WRITE;
/*!40000 ALTER TABLE `toll_event` DISABLE KEYS */;
INSERT INTO `toll_event` VALUES (1,30,'M50',1000,'0000-00-00 00:00:00',1),(2,18,'M50',1001,'0000-00-00 00:00:00',1),(3,3,'M50',1002,'0000-00-00 00:00:00',1),(4,4,'M50',1003,'0000-00-00 00:00:00',1),(5,15,'M50',1004,'0000-00-00 00:00:00',1),(6,31,'M50',1005,'0000-00-00 00:00:00',1),(7,32,'M50',105,'2020-05-08 21:13:40',1),(8,33,'Mad',4124,'2020-05-08 21:13:49',1),(9,12,'M50',10044,'2020-05-08 21:54:16',1),(10,13,'M50',99,'2020-05-08 22:11:40',1),(11,13,'M50',88,'2020-05-08 22:14:53',1),(12,34,'M50',77,'2020-05-08 22:18:42',1);
/*!40000 ALTER TABLE `toll_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `unprocessed_tolls`
--

DROP TABLE IF EXISTS `unprocessed_tolls`;
/*!50001 DROP VIEW IF EXISTS `unprocessed_tolls`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `unprocessed_tolls` (
  `customer_id` tinyint NOT NULL,
  `customer_name` tinyint NOT NULL,
  `customer_address` tinyint NOT NULL,
  `tollbooth_id` tinyint NOT NULL,
  `vehicle_registration` tinyint NOT NULL,
  `vehicle_type` tinyint NOT NULL,
  `cost` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `vehicle`
--

DROP TABLE IF EXISTS `vehicle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vehicle` (
  `vehicle_id` int(11) NOT NULL AUTO_INCREMENT,
  `vehicle_registration` varchar(20) DEFAULT NULL,
  `vehicle_type` varchar(15) DEFAULT NULL,
  `valid` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`vehicle_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehicle`
--

LOCK TABLES `vehicle` WRITE;
/*!40000 ALTER TABLE `vehicle` DISABLE KEYS */;
INSERT INTO `vehicle` VALUES (1,'151DL200','Bus',1),(2,'152DL345','Bus',1),(3,'161C3457','Car',1),(4,'181MH3456','Truck',1),(5,'181MH3458','Car',1),(6,'181MH3459','Car',1),(7,'181MH3461','Bus',1),(8,'191LH1111','Car',1),(9,'191LH1112','Car',1),(10,'191LH1113','Truck',1),(11,'191LH1114','Truck',1),(12,'192D33457','Truck',1),(13,'201CN3456','Bus',1),(14,'201CN3457','Car',1),(15,'201LH3025','Truck',1),(16,'201LH304','Car',1),(17,'201LH305','Car',1),(18,'201LH306','Bus',1),(19,'201LH3064','Car',1),(20,'201LH307','Car',1),(21,'201LH3076','Car',1),(22,'201LH308','Truck',1),(23,'201LH3083','Truck',1),(24,'201LH309','Car',1),(25,'201LH310','Truck',1),(26,'201LH311','Truck',1),(27,'201LH312','Truck',1),(28,'201LH355','Car',1),(29,'201LH777','Bus',1),(30,'151MN666','Car',1),(31,'Happy',NULL,0),(32,'blaw',NULL,0),(33,'dwad',NULL,0),(34,'XXXX',NULL,0);
/*!40000 ALTER TABLE `vehicle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehicle_type_cost`
--

DROP TABLE IF EXISTS `vehicle_type_cost`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vehicle_type_cost` (
  `vehicle_type` varchar(15) DEFAULT NULL,
  `cost` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehicle_type_cost`
--

LOCK TABLES `vehicle_type_cost` WRITE;
/*!40000 ALTER TABLE `vehicle_type_cost` DISABLE KEYS */;
INSERT INTO `vehicle_type_cost` VALUES ('Car',1.9),('Bus',3.4),('Truck',4.8);
/*!40000 ALTER TABLE `vehicle_type_cost` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `unprocessed_tolls`
--

/*!50001 DROP TABLE IF EXISTS `unprocessed_tolls`*/;
/*!50001 DROP VIEW IF EXISTS `unprocessed_tolls`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `unprocessed_tolls` AS select `customer_vehicle`.`customer_id` AS `customer_id`,`customer`.`customer_name` AS `customer_name`,`customer`.`customer_address` AS `customer_address`,`toll_event`.`tollbooth_id` AS `tollbooth_id`,`vehicle`.`vehicle_registration` AS `vehicle_registration`,`vehicle`.`vehicle_type` AS `vehicle_type`,`vehicle_type_cost`.`cost` AS `cost` from ((((`toll_event` join `customer_vehicle` on((`toll_event`.`vehicle_id` = `customer_vehicle`.`vehicle_id`))) join `customer` on((`customer_vehicle`.`customer_id` = `customer`.`customer_id`))) join `vehicle` on((`toll_event`.`vehicle_id` = `vehicle`.`vehicle_id`))) join `vehicle_type_cost` on((`vehicle`.`vehicle_type` = `vehicle_type_cost`.`vehicle_type`))) where (`toll_event`.`processed` = 0) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-08 23:48:43
