package com.dkit.gd2.vilandasmorrissey.client;

import com.dkit.gd2.vilandasmorrissey.core.JsonBuilder;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author Vilandas Morrissey D00218436
 */
public class TempGenerators
{
    public void tempAssignCustomerVehicles()
    {
        Random rM = new Random();

        for(int i = 0; i < 30; i++)
        {
            System.out.println("insert into customer_vehicle(customer_id, vehicle_id) values(\"" +
                    (rM.nextInt(20) + 1) + "\", \"" + (i+1) + "\");");
        }
    }

    public void tempGenerateCustomers()
    {
        String[] names = {
                "Lacresha Lunsford",
                "Todd Champion",
                "Valrie Ennals",
                "Nakisha Gormley",
                "Charline Ptacek",
                "Desire Randazzo",
                "Sherley Sarinana",
                "Adalberto Kleist",
                "Buck Cecil",
                "Reid Weatherford",
                "Trish Rhyne",
                "Junita Driscoll",
                "Andy Labelle",
                "Svetlana Stuber",
                "Drema Paro",
                "Randolph Wymer",
                "Mauricio Sweatman",
                "Sebastian Ungar",
                "Shala Delaune",
                "Harvey Matsuura"
        };

        String[] addresses = {
                "452 Rose St. Buckeye, AZ 85326",
                "34 Trusel Street Davenport, IA 52804",
                "9720 New St. Cumberland, RI 02864",
                "69 South Harrison Dr. Asbury Park, NJ 07712",
                "71 Chestnut Rd. Longview, TX 75604",
                "8410 Grandrose Court Ankeny, IA 50023",
                "8303 Roosevelt St. Dorchester, MA 02125",
                "8871 North Berkshire Drive Park Forest, IL 60466",
                "289 East Oak Valley St. Southfield, MI 48076",
                "77 Vernon Court Ooltewah, TN 37363",
                "456 Henry Drive Maineville, OH 45039",
                "9832 West Court Long Beach, NY 11561",
                "9191 8th St. Providence, RI 02904",
                "9397 Warren St. Wake Forest, NC 27587",
                "85 S. Littleton Lane Glendale Heights, IL 60139",
                "8225 South Helen Avenue Staunton, VA 24401",
                "322 Beech Ave. Severna Park, MD 21146",
                "78 W. Constitution Rd. Traverse City, MI 49684",
                "9932 East Wild Horse Ave. Royal Oak, MI 48067",
                "42 Randall Mill Court Kenosha, WI 53140"
        };

        for(int i = 0; i < 20; i++)
        {
            System.out.println("insert into customer(customer_name, customer_address) values(\"" +
                    names[i] + "\", \"" + addresses[i] + "\");");
        }
    }

    public void tempGenerateVehicles()
    {
        Random rM = new Random();
        String[] types = new String[5];
        types[0] = "Car";
        types[1] = "Car";
        types[2] = "Bus";
        types[3] = "Truck";
        types[4] = "Car";

        List<String> vehicles = loadRegistrations();
        for(int i = 0; i < vehicles.size(); i++)
        {
            System.out.println("insert into vehicle(vehicle_registration, vehicle_type) values(\"" +
                    vehicles.get(i) + "\", \"" + types[rM.nextInt(5)] + "\");");
        }
    }

    public List<String> loadRegistrations()
    {
        List<String> vehicles = new ArrayList<>();
        try(Scanner scanner = new Scanner(new BufferedReader(new FileReader("Vehicles.csv"))))
        {
            scanner.useDelimiter("\\r");
            while(scanner.hasNext())
            {
                String registration = scanner.next();
                vehicles.add(registration);
            }
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        return vehicles;
    }

    public void demoJsonBuilder()
    {
        JsonBuilder builder = new JsonBuilder();
        builder.addProperty("PacketType", "Command");
        builder.addProperty("Test int", 20 );
        builder.addProperty("Vehicles");
        builder.addValue("Vehicles", "10262");
        builder.addValue("Vehicles", "102M2");
        builder.addValue("Vehicles", 1111);
        builder.addProperty("Null");
        builder.addProperty("FinalString", "Yay");
        builder.addProperty("Vehicles2");

        JsonBuilder builder1 = new JsonBuilder();
        builder1.addProperty("id", "123");
        builder1.addProperty("Something int", 222);
        builder.addValue("Vehicles2", builder1);

        builder.addProperty("I don't know", "yup");

        builder.addProperty("Vehicles3");
        JsonBuilder builder2 = new JsonBuilder();
        JsonBuilder builder3 = new JsonBuilder();
        builder3.addProperty("Vehicles");
        builder2.addProperty("Vehicles");
        builder2.addValue("Vehicles", "10262");
        builder2.addValue("Vehicles", "102M2");
        builder2.addValue("Vehicles", 1111);
        builder3.addValue("Vehicles", builder2);
        builder.addValue("Vehicles3", builder3);

        builder.addProperty("I don't know something else", "yup");

        System.out.println(builder);;
    }
}
