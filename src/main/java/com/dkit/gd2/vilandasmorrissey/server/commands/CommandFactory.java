package com.dkit.gd2.vilandasmorrissey.server.commands;

import com.dkit.gd2.vilandasmorrissey.core.ServiceDetails.ECommands;

/**
 *
 * @author Vilandas Morrissey D00218436
 */
public class CommandFactory
{
    public Command createCommand(ECommands command)
    {
        Command c = null;
        switch(command)
        {
            case GET_REGISTERED_VEHICLES:
                c = new GetRegisteredVehiclesCommand();
                break;
            case HEARTBEAT:
                c = new HeartbeatCommand();
                break;
            case REGISTER_VALID_TOLL_EVENT:
                c = new RegisterValidTollCommand();
                break;
            case REGISTER_INVALID_TOLL_EVENT:
                c = new RegisterInvalidTollCommand();
                break;
            default:
                //Should not reach
                System.err.println("CommandFactory Error: Could not create command - " + command);
        }
        return c;
    }
}
