drop database OOPCA6;
create database OOPCA6;
use OOPCA6;

create table customer(
customer_id int auto_increment primary key,
customer_name varchar(50),
customer_address varchar(150)
);

create table vehicle(
vehicle_id int auto_increment primary key,
vehicle_registration varchar(20), vehicle_type varchar(15),
valid boolean
);

create table customer_vehicle(
customer_id int,
vehicle_id int,
foreign key (customer_id) references customer(customer_id),
foreign key (vehicle_id) references vehicle(vehicle_id)
);

create table vehicle_type_cost(
vehicle_type varchar(15),
cost double
);

create table toll_event(
event_id int auto_increment primary key,
vehicle_id int,
tollbooth_id varchar(12),
image_id bigint,
timestamp datetime,
processed boolean not null default 0,
foreign key (vehicle_id) references vehicle(vehicle_id)
);

create view unprocessed_tolls as
select customer_id, customer_name, customer_address, tollbooth_id, vehicle_registration, vehicle_type, cost
from toll_event
inner join customer_vehicle using(vehicle_id)
inner join customer using(customer_id)
inner join vehicle using(vehicle_id)
inner join vehicle_type_cost using(vehicle_type)
where processed = 0;

insert into vehicle_type_cost values ("Car", 1.90);
insert into vehicle_type_cost values ("Bus", 3.40);
insert into vehicle_type_cost values ("Truck", 4.80);

insert into vehicle(vehicle_registration, vehicle_type, valid) values("151DL200", "Bus", true);
insert into vehicle(vehicle_registration, vehicle_type, valid) values("152DL345", "Bus", true);
insert into vehicle(vehicle_registration, vehicle_type, valid) values("161C3457", "Car", true);
insert into vehicle(vehicle_registration, vehicle_type, valid) values("181MH3456", "Truck", true);
insert into vehicle(vehicle_registration, vehicle_type, valid) values("181MH3458", "Car", true);
insert into vehicle(vehicle_registration, vehicle_type, valid) values("181MH3459", "Car", true);
insert into vehicle(vehicle_registration, vehicle_type, valid) values("181MH3461", "Bus", true);
insert into vehicle(vehicle_registration, vehicle_type, valid) values("191LH1111", "Car", true);
insert into vehicle(vehicle_registration, vehicle_type, valid) values("191LH1112", "Car", true);
insert into vehicle(vehicle_registration, vehicle_type, valid) values("191LH1113", "Truck", true);
insert into vehicle(vehicle_registration, vehicle_type, valid) values("191LH1114", "Truck", true);
insert into vehicle(vehicle_registration, vehicle_type, valid) values("192D33457", "Truck", true);
insert into vehicle(vehicle_registration, vehicle_type, valid) values("201CN3456", "Bus", true);
insert into vehicle(vehicle_registration, vehicle_type, valid) values("201CN3457", "Car", true);
insert into vehicle(vehicle_registration, vehicle_type, valid) values("201LH3025", "Truck", true);
insert into vehicle(vehicle_registration, vehicle_type, valid) values("201LH304", "Car", true);
insert into vehicle(vehicle_registration, vehicle_type, valid) values("201LH305", "Car", true);
insert into vehicle(vehicle_registration, vehicle_type, valid) values("201LH306", "Bus", true);
insert into vehicle(vehicle_registration, vehicle_type, valid) values("201LH3064", "Car", true);
insert into vehicle(vehicle_registration, vehicle_type, valid) values("201LH307", "Car", true);
insert into vehicle(vehicle_registration, vehicle_type, valid) values("201LH3076", "Car", true);
insert into vehicle(vehicle_registration, vehicle_type, valid) values("201LH308", "Truck", true);
insert into vehicle(vehicle_registration, vehicle_type, valid) values("201LH3083", "Truck", true);
insert into vehicle(vehicle_registration, vehicle_type, valid) values("201LH309", "Car", true);
insert into vehicle(vehicle_registration, vehicle_type, valid) values("201LH310", "Truck", true);
insert into vehicle(vehicle_registration, vehicle_type, valid) values("201LH311", "Truck", true);
insert into vehicle(vehicle_registration, vehicle_type, valid) values("201LH312", "Truck", true);
insert into vehicle(vehicle_registration, vehicle_type, valid) values("201LH355", "Car", true);
insert into vehicle(vehicle_registration, vehicle_type, valid) values("201LH777", "Bus", true);
insert into vehicle(vehicle_registration, vehicle_type, valid) values("151MN666", "Car", true);
insert into vehicle(vehicle_registration, vehicle_type, valid) values("Happy", NULL, false);

insert into toll_event(tollbooth_id, vehicle_id, image_id, timestamp) VALUES ("M50", 30, 1000, 2020-05-08);
insert into toll_event(tollbooth_id, vehicle_id, image_id, timestamp) VALUES ("M50", 18, 1001, 2020-05-08);
insert into toll_event(tollbooth_id, vehicle_id, image_id, timestamp) VALUES ("M50", 3, 1002, 2020-05-02);
insert into toll_event(tollbooth_id, vehicle_id, image_id, timestamp) VALUES ("M50", 4, 1003, 2020-05-03);
insert into toll_event(tollbooth_id, vehicle_id, image_id, timestamp) VALUES ("M50", 15, 1004, 2020-05-08);
insert into toll_event(tollbooth_id, vehicle_id, image_id, timestamp) VALUES ("M50", 31, 1005, 2020-05-08);


insert into customer(customer_name, customer_address) values("Lacresha Lunsford", "452 Rose St. Buckeye, AZ 85326");
insert into customer(customer_name, customer_address) values("Todd Champion", "34 Trusel Street Davenport, IA 52804");
insert into customer(customer_name, customer_address) values("Valrie Ennals", "9720 New St. Cumberland, RI 02864");
insert into customer(customer_name, customer_address) values("Nakisha Gormley", "69 South Harrison Dr. Asbury Park, NJ 07712");
insert into customer(customer_name, customer_address) values("Charline Ptacek", "71 Chestnut Rd. Longview, TX 75604");
insert into customer(customer_name, customer_address) values("Desire Randazzo", "8410 Grandrose Court Ankeny, IA 50023");
insert into customer(customer_name, customer_address) values("Sherley Sarinana", "8303 Roosevelt St. Dorchester, MA 02125");
insert into customer(customer_name, customer_address) values("Adalberto Kleist", "8871 North Berkshire Drive Park Forest, IL 60466");
insert into customer(customer_name, customer_address) values("Buck Cecil", "289 East Oak Valley St. Southfield, MI 48076");
insert into customer(customer_name, customer_address) values("Reid Weatherford", "77 Vernon Court Ooltewah, TN 37363");
insert into customer(customer_name, customer_address) values("Trish Rhyne", "456 Henry Drive Maineville, OH 45039");
insert into customer(customer_name, customer_address) values("Junita Driscoll", "9832 West Court Long Beach, NY 11561");
insert into customer(customer_name, customer_address) values("Andy Labelle", "9191 8th St. Providence, RI 02904");
insert into customer(customer_name, customer_address) values("Svetlana Stuber", "9397 Warren St. Wake Forest, NC 27587");
insert into customer(customer_name, customer_address) values("Drema Paro", "85 S. Littleton Lane Glendale Heights, IL 60139");
insert into customer(customer_name, customer_address) values("Randolph Wymer", "8225 South Helen Avenue Staunton, VA 24401");
insert into customer(customer_name, customer_address) values("Mauricio Sweatman", "322 Beech Ave. Severna Park, MD 21146");
insert into customer(customer_name, customer_address) values("Sebastian Ungar", "78 W. Constitution Rd. Traverse City, MI 49684");
insert into customer(customer_name, customer_address) values("Shala Delaune", "9932 East Wild Horse Ave. Royal Oak, MI 48067");
insert into customer(customer_name, customer_address) values("Harvey Matsuura", "42 Randall Mill Court Kenosha, WI 53140");


insert into customer_vehicle(customer_id, vehicle_id) values("12", "1");
insert into customer_vehicle(customer_id, vehicle_id) values("8", "2");
insert into customer_vehicle(customer_id, vehicle_id) values("7", "3");
insert into customer_vehicle(customer_id, vehicle_id) values("7", "4");
insert into customer_vehicle(customer_id, vehicle_id) values("13", "5");
insert into customer_vehicle(customer_id, vehicle_id) values("18", "6");
insert into customer_vehicle(customer_id, vehicle_id) values("5", "7");
insert into customer_vehicle(customer_id, vehicle_id) values("13", "8");
insert into customer_vehicle(customer_id, vehicle_id) values("8", "9");
insert into customer_vehicle(customer_id, vehicle_id) values("3", "10");
insert into customer_vehicle(customer_id, vehicle_id) values("14", "11");
insert into customer_vehicle(customer_id, vehicle_id) values("4", "12");
insert into customer_vehicle(customer_id, vehicle_id) values("2", "13");
insert into customer_vehicle(customer_id, vehicle_id) values("15", "14");
insert into customer_vehicle(customer_id, vehicle_id) values("7", "15");
insert into customer_vehicle(customer_id, vehicle_id) values("10", "16");
insert into customer_vehicle(customer_id, vehicle_id) values("6", "17");
insert into customer_vehicle(customer_id, vehicle_id) values("17", "18");
insert into customer_vehicle(customer_id, vehicle_id) values("20", "19");
insert into customer_vehicle(customer_id, vehicle_id) values("10", "20");
insert into customer_vehicle(customer_id, vehicle_id) values("11", "21");
insert into customer_vehicle(customer_id, vehicle_id) values("1", "22");
insert into customer_vehicle(customer_id, vehicle_id) values("6", "23");
insert into customer_vehicle(customer_id, vehicle_id) values("19", "24");
insert into customer_vehicle(customer_id, vehicle_id) values("9", "25");
insert into customer_vehicle(customer_id, vehicle_id) values("17", "26");
insert into customer_vehicle(customer_id, vehicle_id) values("13", "27");
insert into customer_vehicle(customer_id, vehicle_id) values("1", "28");
insert into customer_vehicle(customer_id, vehicle_id) values("16", "29");
insert into customer_vehicle(customer_id, vehicle_id) values("18", "30");