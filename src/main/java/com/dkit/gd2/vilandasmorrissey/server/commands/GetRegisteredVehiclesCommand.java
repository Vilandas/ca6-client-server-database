package com.dkit.gd2.vilandasmorrissey.server.commands;

import com.dkit.gd2.vilandasmorrissey.DTOs.Vehicle;
import com.dkit.gd2.vilandasmorrissey.Daos.ITollsDaoInterface;
import com.dkit.gd2.vilandasmorrissey.Daos.MySQLTollsDAO;
import com.dkit.gd2.vilandasmorrissey.Exceptions.DaoException;
import com.dkit.gd2.vilandasmorrissey.core.JsonBuilder;
import com.dkit.gd2.vilandasmorrissey.core.ServiceDetails;

import java.util.ArrayList;
import java.util.List;

import static com.dkit.gd2.vilandasmorrissey.core.ServiceDetails.ECommands.GET_REGISTERED_VEHICLES;
import static com.dkit.gd2.vilandasmorrissey.core.ServiceDetails.ECommands.SERVER_ERROR;

/**
 *
 * @author Vilandas Morrissey D00218436
 */
public class GetRegisteredVehiclesCommand implements Command
{
    @Override
    public String createResponse(ServiceDetails.ECommands command, Object param)
    {
        JsonBuilder builder = new JsonBuilder();
        try
        {
            ITollsDaoInterface ITollsDao = new MySQLTollsDAO();
            List<Vehicle> vehicles = new ArrayList(ITollsDao.getRegisteredVehicles());
            List<Object> registries = new ArrayList();
            for(Vehicle vehicle : vehicles)
            {
                if(vehicle.isValid())
                {
                    registries.add(vehicle.getRegistry());
                }
            }
            builder.addProperty("PacketType", GET_REGISTERED_VEHICLES.getResponse());
            builder.addPropertyWithList("Vehicles", registries);
        }
        catch(DaoException e)
        {
            System.out.println("Could not get Registered Vehicles: " + e.getMessage());
            builder.addProperty("PacketType", SERVER_ERROR.getResponse());
        }
        return builder.toString();
    }
}
