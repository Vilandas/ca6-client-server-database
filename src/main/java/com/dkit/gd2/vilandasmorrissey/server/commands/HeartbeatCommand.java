package com.dkit.gd2.vilandasmorrissey.server.commands;

import com.dkit.gd2.vilandasmorrissey.core.JsonBuilder;
import com.dkit.gd2.vilandasmorrissey.core.ServiceDetails.ECommands;

/**
 *
 * @author Vilandas Morrissey D00218436
 */
public class HeartbeatCommand implements Command
{

    @Override
    public String createResponse(ECommands command, Object param)
    {
        JsonBuilder builder = new JsonBuilder();
        builder.addProperty("PacketType", command.getResponse());
        return builder.toString();
    }
}
