package com.dkit.gd2.vilandasmorrissey.DTOs;


import java.util.ArrayList;

/**
 *
 * @author Vilandas Morrissey D00218436
 */
public class Customer
{
    private String name;
    private String address;
    private ArrayList<TollDetails> tollEvents;

    public Customer(String name, String address)
    {
        this.name = name;
        this.address = address;
        this.tollEvents = new ArrayList();
    }

    public void addToll(String boothID, String vehicleType, String registration, double cost)
    {
        tollEvents.add(new TollDetails(boothID, vehicleType, registration, cost));
    }

    public void printBill()
    {
        double totalCost = 0;
        System.out.println(String.format("Name: %-50s Address: %s", name, address));
        System.out.println(String.format("%-12s %-15s %-20s %s", "Booth ID: ", "Vehicle Type: ", "Registration", "Cost"));
        for(TollDetails tollEvent : tollEvents)
        {
            System.out.println(tollEvent);
            totalCost += tollEvent.getCost();
        }
        System.out.println(String.format("Total Cost: € %.2f", totalCost));
    }

    /**
     * FOR TESTING ONLY
     * @return total cost of all tolls
     */
    public double getTotalCostForTesting()
    {
        double totalCost = 0;
        for(TollDetails tollEvent : tollEvents)
        {
            System.out.println(tollEvent);
            totalCost += tollEvent.getCost();
        }
        return totalCost;
    }
}
