package com.dkit.gd2.vilandasmorrissey.Daos;

import com.dkit.gd2.vilandasmorrissey.DTOs.Customer;
import com.dkit.gd2.vilandasmorrissey.DTOs.TollEvent;
import com.dkit.gd2.vilandasmorrissey.DTOs.Vehicle;
import com.dkit.gd2.vilandasmorrissey.Exceptions.DaoException;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Vilandas Morrissey D00218436
 */
public class MySQLTollsDAO extends MySqlDao implements ITollsDaoInterface
{

    @Override
    public List<Vehicle> getRegisteredVehicles() throws DaoException
    {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Vehicle> vehicleList = new ArrayList();

        try
        {
            con = this.getConnection();

            String query = "select vehicle_registration, valid from vehicle";
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();

            while(rs.next())
            {
                String registration = rs.getString("vehicle_registration");
                boolean valid = rs.getBoolean("valid");
                vehicleList.add(new Vehicle(registration, valid));
            }
        }
        catch(SQLException e)
        {
            throw new DaoException("getRegisteredVehicles() " + e.getMessage());
        }
        finally
        {
            try
            {
                if(rs != null)
                {
                    rs.close();
                }
                if(ps != null)
                {
                    ps.close();
                }
                if(con != null)
                {
                    freeConnection(con);
                }
            }
            catch(SQLException e)
            {
                throw new DaoException("getRegisteredVehicles() " + e.getMessage());
            }
        }
        return vehicleList;
    }

    @Override
    public HashMap<Integer, Customer> getUnprocessedTolls() throws DaoException
    {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        HashMap<Integer, Customer> customerList = new HashMap();

        try
        {
            con = this.getConnection();

            String query = "select * from unprocessed_tolls";
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();

            while(rs.next())
            {
                int id = rs.getInt("customer_id");
                String tollbooth_id = rs.getString("tollbooth_id");
                String registration = rs.getString("vehicle_registration");
                String type = rs.getString("vehicle_type");
                double cost = rs.getDouble("cost");
                if(customerList.containsKey(id))
                {
                    customerList.get(id).addToll(tollbooth_id, type, registration, cost);
                }
                else
                {
                    String name = rs.getString("customer_name");
                    String address = rs.getString("customer_address");
                    Customer customer = new Customer(name, address);
                    customer.addToll(tollbooth_id, type, registration, cost);
                    customerList.put(id, customer);
                }
            }
        }
        catch(SQLException e)
        {
            throw new DaoException("getUnprocessedTolls() " + e.getMessage());
        }
        finally
        {
            try
            {
                if(rs != null)
                {
                    rs.close();
                }
                if(ps != null)
                {
                    ps.close();
                }
                if(con != null)
                {
                    freeConnection(con);
                }
            }
            catch(SQLException e)
            {
                throw new DaoException("getUnprocessedTolls() " + e.getMessage());
            }
        }
        return customerList;
    }

    @Override
    public void markTollsAsProcessed() throws DaoException
    {
        Connection con = null;
        PreparedStatement ps = null;

        try
        {
            con = this.getConnection();
            String query = "update toll_event SET processed = 1 WHERE processed = 0";
            ps = con.prepareStatement(query);
            ps.executeUpdate();
        }
        catch(SQLException e)
        {
            throw new DaoException("markTollsAsProcessed() " + e.getMessage());
        }
        finally
        {
            try
            {
                if(ps != null)
                {
                    ps.close();
                }
                if(con != null)
                {
                    freeConnection(con);
                }
            }
            catch(SQLException e)
            {
                throw new DaoException("markTollsAsProcessed() " + e.getMessage());
            }
        }
    }

    @Override
    public void registerTollEvent(TollEvent tollEvent) throws DaoException
    {
        Connection con = null;
        PreparedStatement ps = null;

        try
        {
            con = this.getConnection();
            String query = "insert into toll_event(tollbooth_id, vehicle_id, image_id, timestamp) VALUES (?, ?, ?, ?)";
            ps = con.prepareStatement(query);
            ps.setString(1, tollEvent.getTollboothID());
            ps.setInt(2, tollEvent.getVehicleID());
            ps.setLong(3, tollEvent.getImageID());
            ps.setTimestamp(4, tollEvent.getTime());
            ps.executeUpdate();
        }
        catch(SQLException e)
        {
            throw new DaoException("registerTollEvent() " + e.getMessage());
        }
        finally
        {
            try
            {
                if(ps != null)
                {
                    ps.close();
                }
                if(con != null)
                {
                    freeConnection(con);
                }
            }
            catch(SQLException e)
            {
                throw new DaoException("registerTollEvent() " + e.getMessage());
            }
        }
    }

    @Override
    public void registerVehicleRegistration(String registration, boolean valid) throws DaoException
    {
        Connection con = null;
        PreparedStatement ps = null;

        try
        {
            con = this.getConnection();
            String query = "insert into vehicle(vehicle_registration, valid) VALUES (?, ?)";
            ps = con.prepareStatement(query);
            ps.setString(1, registration);
            ps.setBoolean(2, valid);
            ps.executeUpdate();
        }
        catch(SQLException e)
        {
            throw new DaoException("registerVehicleRegistration() " + e.getMessage());
        }
        finally
        {
            try
            {
                if(ps != null)
                {
                    ps.close();
                }
                if(con != null)
                {
                    freeConnection(con);
                }
            }
            catch(SQLException e)
            {
                throw new DaoException("registerVehicleRegistration() " + e.getMessage());
            }
        }
    }
}
