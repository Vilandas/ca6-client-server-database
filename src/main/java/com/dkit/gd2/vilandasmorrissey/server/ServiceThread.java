package com.dkit.gd2.vilandasmorrissey.server;

import com.dkit.gd2.vilandasmorrissey.DTOs.TollEvent;
import com.dkit.gd2.vilandasmorrissey.DTOs.Vehicle;
import com.dkit.gd2.vilandasmorrissey.Daos.ITollsDaoInterface;
import com.dkit.gd2.vilandasmorrissey.Daos.MySQLTollsDAO;
import com.dkit.gd2.vilandasmorrissey.Exceptions.DaoException;
import com.dkit.gd2.vilandasmorrissey.server.commands.Command;
import com.dkit.gd2.vilandasmorrissey.server.commands.CommandFactory;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.Timestamp;
import java.util.*;

import static com.dkit.gd2.vilandasmorrissey.core.ServiceDetails.*;

/**
 *
 * @author Vilandas Morrissey D00218436
 */
public class ServiceThread extends Thread
{
    private Socket dataSocket;
    private Scanner input;
    private PrintWriter output;
    private static HashMap<Vehicle, Integer> vehiclesList;

    public ServiceThread(ThreadGroup group, String name, Socket dataSocket)
    {
        super(group, name);
        try
        {
            this.dataSocket = dataSocket;
            this.input = new Scanner(new InputStreamReader(this.dataSocket.getInputStream()));
            this.output = new PrintWriter(this.dataSocket.getOutputStream(), true);
            vehiclesList = new HashMap();
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Update the server list of vehicles.
     */
    private synchronized void getVehiclesList()
    {
        if(vehiclesList.isEmpty())
        {
            try
            {
                ITollsDaoInterface ITollsDao = new MySQLTollsDAO();
                List<Vehicle> list = ITollsDao.getRegisteredVehicles();
                for(int i = 0; i < list.size(); i++)
                {
                    vehiclesList.put(list.get(i), (i+1));
                }

            } catch (DaoException e)
            {
                System.out.println(e.getMessage());
            }
        }
    }

    /**
     * Add an invalid vehicle registration to the server vehicle list and insert it into the database
     * @param vehicle
     * @return
     */
    private boolean insertRegistration(Vehicle vehicle)
    {
        vehiclesList.put(vehicle, vehiclesList.size());
        ITollsDaoInterface ITolls = new MySQLTollsDAO();
        try
        {
            ITolls.registerVehicleRegistration(vehicle.getRegistry(), vehicle.isValid());
            return true;
        } catch (DaoException e)
        {
            System.out.println(e.getMessage());
            return false;
        }
    }

    /**
     * Check with the vehicle list to retrieve the id of the vehicle.
     * If it does not exist, it is inserted into the vehicle list.
     * @param vehicle The vehicle to check the id for.
     * @return vehicle id
     */
    private synchronized int getID(Vehicle vehicle)
    {
        if(vehiclesList.containsKey(vehicle))
        {
            return vehiclesList.get(vehicle);
        }
        insertRegistration(vehicle);
        return vehiclesList.size();
    }

    /**
     * Get the next line in the scanner and print it
     * @return the line
     */
    private String getAndPrintLine()
    {
        String line = input.nextLine();
        System.out.println(line);
        return line;
    }

    /**
     * Parse JSON input into TollEvent object
     * @return TollEvent object
     */
    private TollEvent parseToll()
    {
        String boothID = parseJsonLine(getAndPrintLine());
        Vehicle vehicle = new Vehicle(parseJsonLine(getAndPrintLine()), false);
        Long imageID = Long.parseLong(parseJsonLine(getAndPrintLine()));
        Timestamp dateTime = Timestamp.valueOf(parseJsonLine(getAndPrintLine()));
        TollEvent tollEvent = new TollEvent(boothID, vehicle.getRegistry(), imageID, dateTime);
        tollEvent.setVehicleID(getID(vehicle));
        return tollEvent;
    }

    /**
     * Parse a JSON formatted line to it's property value.
     * @param line The String line to parse
     * @return String value from the property
     */
    private String parseJsonLine(String line)
    {
        line = line.trim();
        line = line.replaceAll("\"", "");
        line = line.substring(line.indexOf(":") + 2);
        if(line.charAt(line.length() - 1) == ',')
        {
            line = line.substring(0, line.length() - 1);
        }
        return line;
    }

    /**
     * Read the rest of the input
     */
    private void finishReadingInput()
    {
        String line = "";
        while(!line.equals("}"))
        {
            line = input.nextLine();
            System.out.println(line);
        }
    }

    /**
     * Check PacketType property for command
     * @return
     */
    private ECommands getCommand()
    {
        String line = "";
        ECommands incomingCommand = ECommands.UNRECOGNISED;
        while(!line.equals("}"))
        {
            line = input.nextLine();
            System.out.println(line);
            if(line.contains("PacketType"))
            {
                return ECommands.valueOf(parseJsonLine(line));
            }
        }
        return incomingCommand;
    }

    /**
     * Create object parameter to pass into creation of command if required
     * @param incomingCommand The command to check and create a parameter for
     * @return Parameter object or null
     */
    private Object createParameterObject(ECommands incomingCommand)
    {
        Object param;
        switch(incomingCommand)
        {
            case REGISTER_VALID_TOLL_EVENT:
            case REGISTER_INVALID_TOLL_EVENT:
                getVehiclesList();
                param = parseToll();
                break;
            default:
                param = null;
        }
        return param;
    }

    /**
     * Create a response to the command using command factory
     * @param incomingCommand
     */
    private void createResponse(ECommands incomingCommand)
    {
        if(!incomingCommand.equals(ECommands.CLOSE))
        {
            String response;
            Object param = createParameterObject(incomingCommand);
            finishReadingInput();
            System.out.println("Received message: " + incomingCommand);
            CommandFactory factory = new CommandFactory();
            Command command = factory.createCommand(incomingCommand);
            if (command != null)
            {
                response = command.createResponse(incomingCommand, param);
            } else
            {
                //Should not reach (ECommand exists but could not create a Command response)
                System.err.println("ServiceThread Error: Could not create command - " + incomingCommand);
                response = ECommands.UNRECOGNISED.getResponse();
            }

            output.println(response);
        }
    }

    @Override
    public void run()
    {
        try
        {
            getVehiclesList();
            ECommands incomingCommand = ECommands.UNRECOGNISED;
            while (!incomingCommand.equals(ECommands.CLOSE))
            {
                incomingCommand = getCommand();
                createResponse(incomingCommand);
            }
            System.out.println("Connection with client closed");
        }
        catch(NoSuchElementException e)
        {
            System.out.println("Client force quit");
        }
        catch(IllegalArgumentException e)
        {
            //Should not be reachable (ECommand does not exit)
            System.err.println("ServiceThread Exception: Unrecognised Command");
        }
        finally
        {
            try
            {
                if (dataSocket != null)
                {
                    dataSocket.close();
                }
            }
            catch(IOException e)
            {
                System.out.println("Unable to disconnect: " + e.getMessage());
                System.exit(1);
            }
        }
    }
}
