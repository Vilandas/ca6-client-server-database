package com.dkit.gd2.vilandasmorrissey.client;

/**
 *
 * @author Vilandas Morrissey D00218436
 */
public class ClientApp
{
    public static void main(String[] args)
    {
        Client client = new Client();
        client.start();

//        TempGenerators temp = new TempGenerators();
//        temp.demoJsonBuilder();
//        temp.tempGenerateVehicles();
//        temp.tempGenerateCustomers();
//        temp.tempAssignCustomerVehicles();

    }
}
