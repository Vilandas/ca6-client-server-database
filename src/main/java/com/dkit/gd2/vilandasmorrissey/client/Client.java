package com.dkit.gd2.vilandasmorrissey.client;

import com.dkit.gd2.vilandasmorrissey.DTOs.Customer;
import com.dkit.gd2.vilandasmorrissey.DTOs.TollEvent;
import com.dkit.gd2.vilandasmorrissey.Daos.ITollsDaoInterface;
import com.dkit.gd2.vilandasmorrissey.Daos.MySQLTollsDAO;
import com.dkit.gd2.vilandasmorrissey.Exceptions.DaoException;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.Scanner;

import static com.dkit.gd2.vilandasmorrissey.core.Colours.*;
import static com.dkit.gd2.vilandasmorrissey.core.ServiceDetails.*;

/**
 *
 * @author Vilandas Morrissey D00218436
 */
public class Client
{
    private String inKey = "\n)> ";
    private Scanner keyboard = new Scanner(System.in);

    /**
     * Puts the thread to sleep for 1 second before checking
     * to see if a response has been received
     */
    private synchronized void waitForResponse()
    {
        try
        {
            Thread.sleep(1000);
        }
        catch(InterruptedException e)
        {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Starts the client loop
     */
    public void start()
    {
        ClientConnectionThread connectionThread = new ClientConnectionThread();
        connectionThread.readTollsFromFile();
        connectionThread.start();

        //Wait to receive a list of valid registrations (1 attempt)
        while(connectionThread.isWaitingForResponse())
        {
            waitForResponse();
        }

        Menu menuChoice = Menu.UPDATED_VALID_VEHICLES_LIST;
        while(!menuChoice.equals(Menu.SAVE_LOCAL_TOLLS_AND_QUIT))
        {
            while(connectionThread.isWaitingForResponse())
            {
                waitForResponse();
            }

            menuChoice = Menu.values()[getChoice()];

            switch(menuChoice)
            {
                case SAVE_LOCAL_TOLLS_AND_QUIT:
                    connectionThread.writeTollsToFile();
                    connectionThread.sendAndQuit(connectionThread.buildJSONPacketType(ECommands.CLOSE));
                    break;

                case UPDATED_VALID_VEHICLES_LIST:
                    connectionThread.sendCommandAndReceive(connectionThread.buildJSONPacketType(ECommands.GET_REGISTERED_VEHICLES));
                    break;

                case HEARTBEAT:
                    connectionThread.sendCommandAndReceive(connectionThread.buildJSONPacketType(ECommands.HEARTBEAT));
                    break;

                case REGISTER_TOLL_EVENT:
                    TollEvent toll = createTollEvent();
                    if(toll !=  null)
                    {
                        connectionThread.addTollEvent(toll);
                    }
                    break;

                case PROCESS_TOLL_EVENT_BILLS:
                    processTolls();
                    break;

                default:
            }
        }
    }

    /**
     * Get a list of tolls that are unprocessed, display the details of each bill
     * and mark them as processed.
     */
    private void processTolls()
    {
        try
        {
            ITollsDaoInterface ITolls = new MySQLTollsDAO();
            HashMap<Integer, Customer> customerList = ITolls.getUnprocessedTolls();
            if(customerList.isEmpty())
            {
                System.out.println("No tolls to process");
                return;
            }
            System.out.println();

            for(Customer customer : customerList.values())
            {
                customer.printBill();
                System.out.println();
            }
            ITolls.markTollsAsProcessed();
        } catch (DaoException e)
        {
            System.out.println("Error: Could not connect to database");
        }
    }

    /**
     * Get user input from user and create a toll event.
     * @return TollEvent or null if the user decided to cancel.
     */
    private TollEvent createTollEvent()
    {
        System.out.println("Please enter the following information or Q in any of the following inputs to quit:");
        System.out.print("\nTollbooth ID: " + inKey);
        String tollID = keyboard.nextLine();
        if(tollID.equalsIgnoreCase("Q")) {
            return null;
        }

        System.out.print("\nVehicle registration: " + inKey);
        String vehicleReg = keyboard.nextLine();
        if(vehicleReg.equalsIgnoreCase("Q") ){
            return null;
        }

        System.out.print("\nImage ID: " + inKey);
        long imageID = getLong();
        if(imageID == -1) {
            return null;
        }

        System.out.print("\nTime: " + inKey);
        Timestamp time = getTime();
        if(time == null) {
            return null;
        }

        return new TollEvent(tollID, vehicleReg, imageID, time);
    }

    /**
     * Convert user input String to Timestamp
     * @return Timestamp from inputted time/now or null if the user decided to cancel.
     */
    private Timestamp getTime()
    {
        String input;
        Timestamp time;
        while(true)
        {
            try
            {
                System.out.println("\nFormat 1: YYYY-MM-DD");
                System.out.println("Format 2: YYYY-MM-DD hh:mm:ss");
                System.out.print("\nEnter \"now\" to use current time OR" +
                        "\nEnter a start date in either format 1 or format 2" + inKey);
                input = keyboard.nextLine();
                if(input.equalsIgnoreCase("Q"))
                {
                    time = null;
                }
                else if(input.equalsIgnoreCase("NOW"))
                {
                    time = Timestamp.valueOf(LocalDateTime.now());
                }
                else
                {
                    time = Timestamp.valueOf(input);
                }
                System.out.println(time);
                return time;
            } catch (DateTimeParseException e)
            {
                System.out.println("Could not translate date");
            }
        }
    }

    /**
     * Display menu options
     */
    private void displayMenu()
    {
        System.out.println("\nPlease enter one of the following options: ");
        for (int i = 0; i < Menu.values().length; i++)
        {
            System.out.println("\t" + ANSI_GREEN + i + ")" + Menu.values()[i].getName() + ANSI_RESET);
        }
        System.out.print("Enter a number" + inKey);
    }

    /**
     * Ask user for input and check if it is in the menu choice range.
     * @return Menu choice number (int)
     */
    private int getChoice()
    {
        while(true)
        {
            displayMenu();
            int number = getInt();

            if (number < 0 || number >= Menu.values().length)
            {
                System.out.println("\nInvalid number");
                continue;
            }
            else return number;
        }
    }

    /**
     * Ask user for input until they enter a valid Integer number
     * @return integer number
     */
    private int getInt()
    {
        String input = keyboard.nextLine();
        while(!isInt(input, false))
        {
            input = keyboard.nextLine();
        }
        return Integer.parseInt(input);
    }

    /**
     * Ask user for input until they enter a valid Long number
     * @return long number
     */
    private long getLong()
    {
        String input = keyboard.nextLine();
        if(input.equalsIgnoreCase("Q"))
        {
            return -1;
        }
        while(!isInt(input, true))
        {
            input = keyboard.nextLine();
            if(input.equalsIgnoreCase("Q"))
            {
                return -1;
            }
        }
        return Long.parseLong(input);
    }

    /**
     * Check to see if an input can be parsed to int or long.
     * @param input String of user inputted numbers/letters
     * @param isLong boolean if true, check to see if the input can be parsed to Long instead of Integer.
     * @return True if the input is numeric and can be parsed. False otherwise.
     */
    private boolean isInt(String input, boolean isLong)
    {
        if (input == null)
        {
            System.out.println("\nInvalid input, numbers only");
            return false;
        }
        try
        {
            if(isLong)
            {
                Long.parseLong(input);
            }
            else
            {
                Integer.parseInt(input);
            }
        } catch (NumberFormatException e)
        {
            System.out.println("\nInvalid input, numbers only");
            return false;
        }
        return true;
    }
}
