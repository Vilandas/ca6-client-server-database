package com.dkit.gd2.vilandasmorrissey.client;

import com.dkit.gd2.vilandasmorrissey.DTOs.TollEvent;
import com.dkit.gd2.vilandasmorrissey.core.JsonBuilder;

import java.io.*;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.*;

import static com.dkit.gd2.vilandasmorrissey.core.ServiceDetails.*;

/**
 * Manages connection between the client and server.
 * @author Vilandas Morrissey D00218436
 */
public class ClientConnectionThread extends Thread
{
    private Socket dataSocket;
    private ArrayList<TollEvent> tollEvents;
    private HashSet<String> vehiclesList;
    private boolean continueRunning;
    private boolean waitingForResponse;
    private boolean endThread;

    private OutputStream out;
    private PrintWriter output;
    private InputStream in;
    private Scanner input;

    public ClientConnectionThread()
    {
        this.tollEvents = new ArrayList();
        this.vehiclesList = new HashSet();
        this.dataSocket = null;
        this.waitingForResponse = true;
        this.endThread = false;
    }

    /**
     * Write toll events to file
     */
    public void writeTollsToFile()
    {
        try(ObjectOutputStream fileOutput = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream("TollEvents.data"))))
        {
            for(TollEvent tollEvent : this.tollEvents)
            {
                fileOutput.writeObject(tollEvent);
            }
        }catch(IOException e)
        {
            System.out.println(e.getMessage());
            System.out.println("Error saving file");
        }
    }

    /**
     * Read Toll events from file
     */
    public void readTollsFromFile()
    {
        try(ObjectInputStream fileInput = new ObjectInputStream(new BufferedInputStream(new FileInputStream("TollEvents.data"))))
        {
            boolean eof = false;
            while (eof == false)
            {
                try
                {
                    TollEvent tollEvent = (TollEvent) fileInput.readObject();
                    this.tollEvents.add(tollEvent);
                } catch (EOFException e) {
                    eof = true;
                } catch (ClassNotFoundException e)
                {
                    System.out.println(e.getMessage());
                }
            }
            System.out.println("Loaded " + this.tollEvents.size() + " Toll Events");
        }catch(IOException e)
        {
            System.out.println(e.getMessage());
            System.out.println("Could not read file or file does not exist");
        }
    }

    /**
     * Add toll event to list. If the vehicles list is empty, we are not connected to the server
     * and waiting for response to false. The response process is managed by run() method
     * @param tollEvent Valid or invalid toll event
     */
    public void addTollEvent(TollEvent tollEvent)
    {
        this.tollEvents.add(tollEvent);
        this.waitingForResponse = true;
        if(this.vehiclesList.isEmpty())
        {
            System.out.println("Toll Event stored locally. Please update vehicle registries to send.");
            this.waitingForResponse = false;
        }
    }

    /**
     * Convert a command to the protocol format for sending to the server.
     * @param command Command to put into a JSON String
     * @return JSON formatted String
     */
    public String buildJSONPacketType(ECommands command)
    {
        JsonBuilder builder = new JsonBuilder();
        builder.addProperty("PacketType", command.toString());
        return builder.toString();
    }

    /**
     * Convert toll event properties to JSON format according to the protocol to send to the server.
     * @param command Command used to generate PacketType
     * @param toll The TollEvent to get properties from
     * @return JSON formatted String
     */
    private String buildJSONRegister(ECommands command, TollEvent toll)
    {
        JsonBuilder builder = new JsonBuilder();
        builder.addProperty("PacketType", command.toString());
        builder.addProperty("TollBoothID", toll.getTollboothID());
        builder.addProperty("Vehicle Registration", toll.getVehicleReg());
        builder.addProperty("Vehicle Image ID", toll.getImageID());
        builder.addProperty("LocalDateTime", toll.getTime());
        return builder.toString();
    }

    /**
     * Parse a JSON formatted line to it's property value.
     * @param line The String line to parse
     * @return String value from the property
     */
    private String parseJsonLine(String line)
    {
        line = line.trim();
        if(line.length() < 3)
        {
            return null;
        }
        line = line.replaceAll("\"", "");
        if(line.charAt(line.length() - 1) == ',')
        {
            line = line.substring(0, line.length() - 1);
        }
        return line;
    }

    /**
     * Check for properties within the JSON response and act upon certain properties.
     * @param line The line to check for valid properties
     */
    private void checkLineContainsProperty(String line)
    {
        if (line.contains("Vehicles"))
        {
            handleVehiclesProperty();
        }
    }

    /**
     * Display and add vehicle registries to a hash set.
     */
    private void handleVehiclesProperty()
    {
        this.vehiclesList = new HashSet();
        String response = "";

        response = this.input.nextLine();
        System.out.println(response);

        while(!response.equals("\t]"))
        {
            response = this.input.nextLine();
            System.out.println(response);
            String parsedLine = parseJsonLine(response);
            if(parsedLine != null)
            {
                this.vehiclesList.add(parsedLine);
            }
        }
    }

    public void sendAndQuit(String jsonMessage)
    {
        if (this.dataSocket != null)
        {
            output.println(jsonMessage);
        }
        this.endThread = true;
        this.continueRunning = false;
    }

    /**
     * Send JSON message to server and wait for response.
     * @param jsonMessage The message to send to the server.
     */
    public void sendCommandAndReceive(String jsonMessage)
    {
        try
        {
            if (this.dataSocket != null)
            {
                output.println(jsonMessage);

                String response = "";
                //Read until final "}" closing bracket
                while (!response.equals("}"))
                {
                    response = this.input.nextLine();
                    System.out.println(response);

                    checkLineContainsProperty(response);
                }
            } else System.out.println("No connection to server");
            this.waitingForResponse = false;
        }
        catch(NoSuchElementException e)
        {
            //Set dataSocket to null and restart while loop in Run() method
            System.out.println("Disconnected from server");
            this.dataSocket = null;
            this.continueRunning = false;
        }
    }

    /**
     * Build the input/output streams for sending and receiving from the server.
     * @throws IOException
     */
    private void buildIOStreams() throws IOException
    {
        this.out = this.dataSocket.getOutputStream();
        this.output = new PrintWriter(new OutputStreamWriter(this.out), true);
        this.in = this.dataSocket.getInputStream();
        this.input = new Scanner(new InputStreamReader(this.in));
    }

    /**
     * Check the vehicle registry from the first toll in the toll list against
     * the valid vehicle registries list, send the proper command to the server (valid, invalid)
     * and remove it from the toll list
     */
    private void checkValidToll()
    {
        String tollJSON;
        if(this.vehiclesList.contains(this.tollEvents.get(0).getVehicleReg()))
        {
            tollJSON = buildJSONRegister(ECommands.REGISTER_VALID_TOLL_EVENT, this.tollEvents.get(0));
        }
        else tollJSON = buildJSONRegister(ECommands.REGISTER_INVALID_TOLL_EVENT, this.tollEvents.get(0));
        sendCommandAndReceive(tollJSON);
        this.tollEvents.remove(0);
    }

    /**
     * If the client has sent something and is waiting for a response, this should be true.
     * @return
     */
    public boolean isWaitingForResponse()
    {
        return this.waitingForResponse;
    }

    /**
     * Continuously attempts to connect to the server. Upon connection, will try to update valid vehicles list.
     * Afterwards will continuously check for TollEvents in the tolls list and process them to the server.
     */
    @Override
    public void run()
    {
        try
        {
            //While loop trying to continuously connect to server
            while (!this.endThread)
            {
                try
                {
                    //Establish a connection with server
                    this.dataSocket = new Socket("localhost", LISTENING_PORT);
                    System.out.println("Connection to server established");

                    buildIOStreams();
                    this.continueRunning = true;
                    sendCommandAndReceive(buildJSONPacketType(ECommands.GET_REGISTERED_VEHICLES));
                    //While loop to continuously
                    while(this.continueRunning)
                    {
                        if(!this.tollEvents.isEmpty() && !this.vehiclesList.isEmpty())
                        {
                            checkValidToll();
                        }
                    }
                }
                catch(ConnectException e)
                {
                    //Connection Refused
                    if(this.waitingForResponse)
                    {
                        this.waitingForResponse = false;
                        System.out.println("Could not establish a connection to server");
                    }
                }
            }
        }
        catch(UnknownHostException e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        catch (IOException e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        catch(NoSuchElementException e)
        {
            e.printStackTrace();
            System.out.println("Error, server thread closed: " + e.getMessage());
        }
        finally
        {
            try
            {
                if(this.dataSocket != null)
                {
                    this.dataSocket.close();
                }
            }
            catch(IOException e)
            {
                System.out.println(e.getMessage());
                System.exit(1);
            }
        }
    }
}
