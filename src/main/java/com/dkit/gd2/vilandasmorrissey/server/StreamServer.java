package com.dkit.gd2.vilandasmorrissey.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import static com.dkit.gd2.vilandasmorrissey.core.ServiceDetails.*;

/**
 *
 * @author Vilandas Morrissey D00218436
 */
public class StreamServer
{
    public static void main(String[] args)
    {
        ServerSocket listeningSocket = null;
        try
        {
            //Set up a connection socket to listen for connections
            listeningSocket = new ServerSocket(LISTENING_PORT);

            //Set up a ThreadGroup to manage all of the clients threads
            ThreadGroup clientThreadGroup = new ThreadGroup("Client Threads");
            clientThreadGroup.setMaxPriority(Thread.currentThread().getPriority() - 1);

            boolean continueRunning = true;
            while(continueRunning)
            {
                //Wait for an incoming connect
                Socket dataSocket = listeningSocket.accept();

                ServiceThread newClient = new ServiceThread(
                        clientThreadGroup, dataSocket.getInetAddress().toString(), dataSocket);
                newClient.start();

                System.out.println("Clients connected: " + clientThreadGroup.activeCount());
            }

        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
        finally
        {
            try
            {
                if (listeningSocket != null)
                {
                    listeningSocket.close();
                }
            }
            catch(IOException e)
            {
                System.out.println("Error closing program: " + e.getMessage());
                System.exit(1);
            }
        }
    }
}
