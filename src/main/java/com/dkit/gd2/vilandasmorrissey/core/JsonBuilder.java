package com.dkit.gd2.vilandasmorrissey.core;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 *
 * @author Vilandas Morrissey D00218436
 */
public class JsonBuilder
{
    private LinkedHashMap<String, List<Object>> jsonMap;
    private int jsonBuilderCount;

    public JsonBuilder()
    {
        this.jsonBuilderCount = 0;
        this.jsonMap = new LinkedHashMap();
    }

    /**
     * Add property with no value
     * @param property String identifier
     */
    public void addProperty(String property)
    {
        if(!jsonMap.containsKey(property))
        {
            jsonMap.put(property, new ArrayList());
        }
        else System.out.println("Property " + property + " already added");
    }

    /**
     * Add property with an array of values
     * @param property String identifier
     * @param values Object array of values
     */
    public void addPropertyWithList(String property, List<Object> values)
    {
        if(!jsonMap.containsKey(property))
        {
            jsonMap.put(property, values);
        }
        else System.out.println("Property " + property + " already added");
    }

    /**
     * Add property with 1 value
     * @param property String identifier
     * @param value Object value
     */
    public void addProperty(String property, Object value)
    {
        if(!jsonMap.containsKey(property))
        {
            ArrayList values = new ArrayList();
            values.add(value);
            jsonMap.put(property, values);
        }
        else System.out.println("Property " + property + " already added");
    }

    /**
     * Add one value to an existing property
     * @param property String identifier
     * @param value Object value
     */
    public void addValue(String property, Object value)
    {
        if(jsonMap.containsKey(property))
        {
            jsonMap.get(property).add(value);
        }
        else System.out.println("Property \"" + property + "\" Not found");
    }

    /**
     * For setting the current index of nested JsonBuilders
     * @param count Current count/index
     */
    private void setJsonBuilderCount(int count)
    {
        this.jsonBuilderCount = count;
    }

    /**
     * Returns a string with a number of tabs ("\t")
     * @param tabs The amount of "tabs" to add/return with
     * @return String with tabs
     */
    private String tabs(int tabs)
    {
        String str = "";
        for(int i = 0; i < tabs; i++)
        {
            str += "\t";
        }
        return str;
    }

    @Override
    public String toString()
    {
        StringBuilder json = new StringBuilder();
        int t = 0; //tabs
        int propertyCount = 0;  //Tells when to stop placing "," commas

        /*
            If this is the first JsonBuilder (no nested JsonBuilder)
            Sets the tab count for starting with
         */
        if(this.jsonBuilderCount != 0)
        {
            json.append("\n");
            t = jsonBuilderCount;
        }
        json.append(tabs(t) + "{");

        t++;
        for(String property : jsonMap.keySet())
        {
            propertyCount++;
            List jArray = jsonMap.get(property);

            //Write the property
            json.append("\n" + tabs(t) + "\"" + property + "\": ");

            //If there is more than one value, display as an array (write [] brackets)
            if(jArray.size() > 1)
            {
                json.append("\n" + tabs(t) + "[\n");
                t++;
            }

            //If there are no values, print "" instead of leaving it empty
            if(jArray.size() == 0)
            {
                json.append("\"\"");
            }
            else
            {
                for (int j = 0; j < jArray.size(); j++)
                {
                    Object value = jArray.get(j);

                    /*
                        If value is a JsonBuilder, increase the count
                        (so it starts with +1 tabs at the start of toString method)
                     */
                    if(value instanceof JsonBuilder)
                    {
                        ((JsonBuilder) value).setJsonBuilderCount(this.jsonBuilderCount + 1);
                    }

                    //If there are multiple values, add required indention (as "[" is on line before)
                    if(jArray.size() > 1)
                    {
                        //If value is string, surround with "" otherwise don't
                        if (value instanceof String)
                        {
                            json.append(tabs(t) + "\"" + value + "\"");
                        } else json.append(tabs(t) + value);

                        //If not last value, add comma ","
                        if (j != jArray.size() - 1)
                        {
                            json.append(",\n");
                        }
                    }
                    else    //If single value
                    {
                        //If value is string, surround with "" otherwise don't
                        if (value instanceof String)
                        {
                            json.append("\"" + value + "\"");
                        } else json.append(value);
                    }
                }

                //After loop, if there were multiple values, add ending "]" bracket
                if (jArray.size() > 1)
                {
                    t--;
                    json.append("\n" + tabs(t) + "]");
                }
            }

            //If not the last property, add comma ","
            if(propertyCount != jsonMap.keySet().size())
            {
                json.append(",");
            }
        }
        t--;
        //Add ending bracket "}"
        json.append("\n" + tabs(t) + "}");
        return json.toString();
    }
}
