package com.dkit.gd2.vilandasmorrissey.server.commands;

import com.dkit.gd2.vilandasmorrissey.DTOs.TollEvent;
import com.dkit.gd2.vilandasmorrissey.Daos.ITollsDaoInterface;
import com.dkit.gd2.vilandasmorrissey.Daos.MySQLTollsDAO;
import com.dkit.gd2.vilandasmorrissey.Exceptions.DaoException;
import com.dkit.gd2.vilandasmorrissey.core.JsonBuilder;
import com.dkit.gd2.vilandasmorrissey.core.ServiceDetails;

import static com.dkit.gd2.vilandasmorrissey.core.ServiceDetails.ECommands.SERVER_ERROR;

/**
 *
 * @author Vilandas Morrissey D00218436
 */
public class RegisterInvalidTollCommand implements Command
{
    @Override
    public String createResponse(ServiceDetails.ECommands command, Object param)
    {
        TollEvent tollEvent = (TollEvent) param;
        JsonBuilder builder = new JsonBuilder();
        try
        {
            ITollsDaoInterface ITollsDao = new MySQLTollsDAO();
            ITollsDao.registerTollEvent(tollEvent);
            builder.addProperty("PacketType", command.getResponse());
        }
        catch(DaoException e)
        {
            System.out.println("Could not add toll event to database: " + e.getMessage());
            builder.addProperty("PacketType", SERVER_ERROR.getResponse());
        }
        return builder.toString();
    }
}
